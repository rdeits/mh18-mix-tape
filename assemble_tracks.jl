push!(LOAD_PATH, ".")

using WavEdit
using Unitful

songs = [
    (fade!(trim(WavData("audio/Michael Jackson-Man in the mirror lyrics-P5vz6iwV38U.wav"), 61u"s", 77u"s")),
     fade!(trim(WavData("audio/Diana Ross Mirror Mirror-Ds4aG8NGCYU.wav"), 180u"s", 196u"s"))),
	(fade!(trim(WavData("audio/Abba - When All Is Said And Done-tUh4u-lYEhM.wav"), 88u"s", 104u"s")), 
     fade!(trim(WavData("audio/Earth, Wind & Fire - And Love Goes On (Audio)-wCwI386yz-g.wav"), 71u"s", 87u"s"))),
	(fade!(trim(WavData("audio/a-ha - Take On Me (Official Video)-djV11Xbc914.wav"), 50u"s", 66u"s")), 
     fade!(trim(WavData("audio/Twisted Sister - We're not gonna take it (With Lyrics)-CQ0ftoiIQxU.wav"),9u"s", 25u"s"))),
	(fade!(trim(WavData("audio/Information Society - Walking Away-hQa-b1HTgTc.wav"), 60u"s", 76u"s")), 
     fade!(trim(WavData("audio/Dire Straits - So Far Away-IHXK9glwFBg.wav"), 40u"s", 56u"s"))),
	(fade!(trim(WavData("audio/Beastie Boys - (You Gotta) Fight For Your Right (To Party)-eBShN8qT4lk.wav"), 80u"s", 96u"s")), 
     fade!(trim(WavData("audio/Christopher Cross   All Right-9I-cxF7jZH8.wav"), 46u"s", 62u"s"))),
	(fade!(trim(WavData("audio/Kenny Loggins - Meet Me Half Way-3pYNd0MSVXs.wav"), 42u"s", 58u"s")), 
     fade!(trim(WavData("audio/Leon Haywood - Believe Half Of What You See (And None Of What You Hear)-NvfPRfy4OjM.wav"), 26u"s", 42u"s"))),
	(fade!(trim(WavData("audio/Dr Hook years from now-EfsPeVVL8zE.wav"), 12u"s", 28u"s")), 
     fade!(trim(WavData("audio/Frank Stallone - Far From Over (OFFICIAL VIDEO)-uAuL_noJLoo.wav"), 34u"s", 50u"s"))),
	(fade!(trim(WavData("audio/Pet Shop Boys - Left To My Own Devices-Ed1tv_gCOUA.wav"), 89u"s", 105u"s")), 
     fade!(trim(WavData("audio/Barbra Streisand   Left in the Dark-st1bTsBQH-0.wav"), 180u"s", 196u"s"))),
	]

trackA = vcat(first.(songs)...)
trackB = vcat(last.(songs)...)
write("audio/combined_track_A.wav", trackA)
write("audio/combined_track_B.wav", trackB)
