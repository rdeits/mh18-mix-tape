__precompile__()

module WavEdit

export WavData, trim, fade!

using WAV
using Unitful

struct WavData{T}
    samples::Matrix{T}
    Fs::Float32
    nbits::UInt16
    opt::Dict{Symbol, Any}
end

samples(s::WavData) = s.samples

duration(s::WavData) = size(s.samples, 1) / s.Fs * u"s"

function WavData(fname::String, normalize=true)
    samples, Fs, nbits, opt = wavread(fname)
    if normalize
        rms = sqrt(mean(samples .^ 2))
        samples ./= 2 * rms
    end
    WavData(samples, Fs, nbits, opt)
end

function Base.write(fname::String, s::WavData)
    wavwrite(s.samples, fname, Fs=s.Fs)
end

function trim(s::WavData, start::Quantity, final::Quantity)
    samples = s.samples[max(1, round(Int, s.Fs * (start / 1u"s"))):min(size(s.samples, 1), round(Int, s.Fs * (final / 1u"s"))), :]
    WavData(samples, s.Fs, s.nbits, s.opt)
end

function fade!(s::WavData, beginning::Quantity=2u"s", ending::Quantity=2u"s")
    N = round(Int, s.Fs * beginning / 1u"s")
    for i in 1:N
        magnitude = (i / N)^2
        s.samples[i, :] .*= magnitude
    end
    N = round(Int, s.Fs * ending / 1u"s")
    for i in 1:N
        magnitude = (i / N)^2
        s.samples[end - i + 1, :] .*= magnitude
    end
    s
end

function Base.vcat(s1::WavData, others::WavData...)
    for other in others
        @assert other.Fs == s1.Fs
        @assert other.nbits == s1.nbits
    end
    WavData(vcat(s1.samples, samples.(others)...), s1.Fs, s1.nbits, s1.opt)
end

end