module vinyl

using Images
using Interpolations
using Parameters

@with_kw mutable struct Config
    groove_spacing::Int = 15
    padding::Int = 4 * groove_spacing
    scaling::Float64 = 0.5
    groove_fraction::Float64 = 0.2
end

# const groove_spacing = 15
# const padding = 4 * groove_spacing
# const scaling = 0.5

function xy_to_sample(x, y, width, rps, fsample, config::Config)
    @unpack groove_spacing, padding, groove_fraction = config
    
    v = [x - width / 2, y - width / 2]
    theta = atan2(v[2], v[1])
    if theta < 0
        theta += 2pi
    end
    r = norm(v)
    groove = (width / 2 - padding - r) / groove_spacing - theta / (2pi)
    if abs(groove - round(groove)) < groove_fraction
        revolutions = (round(Int, groove) + theta / (2pi))
        time = revolutions / rps
        sample = time * fsample
        # round(Int, sample) + 1
        sample + 1
    else
        0
    end
end

function cut_record(samples, fsample, rpm, width, config::Config)
    @unpack scaling = config

    rps = rpm / 60
    
    im = fill(Gray{Float32}(0.0), (width, width)) 
    
    ub = maximum(samples)
    lb = minimum(samples)
    m = median(samples)
    rescale = s -> clamp01nan((s - m) * scaling + 0.5)

    sample_interp = interpolate(samples, BSpline(Cubic(Flat())), OnGrid())
    
    for y in 1:width
        for x in 1:width
            i = xy_to_sample(x, y, width, rps, fsample, config)
            if i >= 1 && i <= length(samples)
                im[x, y] = rescale(sample_interp[i])
            end
        end
    end
    im
end

function play_record(im, fsample, rpm, config::Config, duration=120)
    @unpack groove_spacing, padding = config
    width = size(im, 1)
    rps = rpm / 60
    nsamples = round(Int, duration * fsample)
    samples = zeros(nsamples)
    for i in 1:nsamples
        time = i / fsample
        revolutions = time * rps
        theta = revolutions * 2 * pi
        r = (width / 2 - padding) - groove_spacing * revolutions
        x = round(Int, width / 2 + r * cos(theta))
        y = round(Int, width / 2 + r * sin(theta))
        samples[i] = Gray(im[x, y]) - 0.5
    end
    samples
    # (samples .- minimum(samples)) ./ (maximum(samples) - minimum(samples))
end

end