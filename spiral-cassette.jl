push!(LOAD_PATH, ".")

using FileIO, Images, DSP, WavEdit, Unitful, WAV

import vinyl

trackA = WavData("audio/combined_track_A.wav")
trackB = WavData("audio/combined_track_B.wav")

duration = WavEdit.duration(trackA)
@assert isapprox(WavEdit.duration(trackB), duration, rtol=1e-2)

trackA_samples = filt(digitalfilter(
        Lowpass(2000; fs=trackA.Fs),
        FIRWindow(hanning(64))), trackA.samples[:, 1]);
trackB_samples = filt(digitalfilter(
        Lowpass(2000; fs=trackA.Fs),
        FIRWindow(hanning(64))), trackB.samples[:, 1]);

const rpm = 33
const width = 4500
config = vinyl.Config(groove_fraction=0.4, 
    padding=round(Int, width / 10),
    scaling=0.99 / maximum(trackA_samples))

punkrock = Gray.(load("img/punkrock_56.png"))
punkrock = punkrock[:, end:-1:1]
# imfilter!(punkrock, punkrock, Kernel.gaussian(3))
punkrock = imresize(punkrock, width, width)
punkrock .= 1 .- (1 .- punkrock) .* 0.5

im1_clean = vinyl.cut_record(trackA_samples, trackA.Fs, rpm, width, config)
im2_clean = vinyl.cut_record(reverse(trackB_samples), trackB.Fs, rpm, width, config)

im1 = punkrock .* im1_clean .+ (1 .- punkrock) .* im2_clean
im2 = punkrock .* im2_clean .+ (1 .- punkrock) .* im1_clean

combined = hcat(im1[:, end:-1:1], im2)
g = config.groove_spacing
r = round(Int, 2 * config.groove_fraction * g/2)
combined[(end - config.padding - r):(end - config.padding + r), round(Int, width/2):(end-round(Int, width/2))] = 0.5
save("build/combined.png", combined)

recovered_left = combined[:, 1:size(combined, 2)÷2]
recovered_right = combined[:, (size(combined, 2)÷2 + 1):end]

save("build/difference.png", recovered_left .- recovered_right[:, end:-1:1])

Fs_play = 8000
wavwrite(vinyl.play_record(recovered_left[:, end:-1:1], 
        Fs_play, rpm, config, duration/1u"s"), 
    "build/recovered_left.wav", Fs=Fs_play)
wavwrite(reverse(vinyl.play_record(recovered_right, 
            Fs_play, rpm, config, duration/1u"s")), 
    "build/recovered_right.wav", Fs=Fs_play)


